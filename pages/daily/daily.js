const app = getApp();
//引入插件：微信同声传译
const plugin = requirePlugin("WechatSI");
const manager = plugin.getRecordRecognitionManager();
var util=require('../../utils/util.js')
const audio=wx.createInnerAudioContext({})
Page({
  data: {
    // 输入框显示值
    search:'',
    //任何输入框的值
    inputText:'',
    focus:false,
    //任务清单数据模型
    todos:[
      {name:'早起一杯水',completed:false},
      {name:'记单词20分钟',completed:false},
    ],
    //未完成项目数
    leftcount:2,
    //语音
    status:'语音输入',
    //键盘高度
    height:0,
    //隐藏事件
    addContainerHide:false,
    InputContainerHide:true,
    getfocus:false,
    inputhide:true,
    btnhide:true,
    inputDisable:false,
    unfoldhide:false,
    user:'',
    Time:''
  },
  gettime(){
    console.log(util.myhour(new Date())[0])
    let currentTime=util.myhour(new Date())[0]
    if(currentTime<=10){
      this.setData({
        Time:"早上好"
      })
    }else if(currentTime<=14){
      this.setData({
        Time:'中午好'
      })
    }else if(currentTime<=18){
      this.setData({
        Time:'下午好'
      })

    }else{
      this.setData({
        Time:'晚上好'
      })
    }
    
  },
  onLoad: function (options) {
    // const innerAudioContext = wx.createInnerAudioContext();//新建一个createInnerAudioContext();
    // innerAudioContext.autoplay = true;//音频自动播放设置
    // innerAudioContext.src = '/audio/notice.mp3';//链接到音频的地址
    // innerAudioContext.onPlay(() => {});//播放音效
    // innerAudioContext.onError((res) => {//打印错误
    //   console.log(res.errMsg);//错误信息
    //   console.log(res.errCode);//错误码
    // })
  
   this.gettime()
    wx.getUserInfo({
      complete: (res) => {
        this.setData({
          user:res.userInfo.nickName
        })
      },
    })
    audio.src="https://666c-florence-60ejp-1300496285.tcb.qcloud.la/music/%E6%8F%90%E7%A4%BA%E9%9F%B3/demo1.mp3?sign=415e3993719e6b9e3c3f241bbe50da03&t=1587818706"
    wx.getStorage({
      key: 'daily',
      success:(res)=>{
        console.log(res)
        this.setData({
          todos:res.data.todos,
          leftcount:res.data.leftcount
        })
      }
    })
  },
   //添加任务内容
  addlist(){
    if(!this.data.inputText) return
    var todos=this.data.todos
    todos.unshift({name:this.data.inputText,completed:false})
    this.data.inputText=''
    this.setData({
      todos:todos,
      leftcount:this.data.leftcount+1,
      search:'',
      inputText:'',
      inputhide:true,
      InputContainerHide:true,
      addContainerHide:false,
      btnhide:true,
      unfoldhide:false
    })
    wx.setStorage({
      key:'daily',
      data:this.data,
      success(e){}
    })
  },
  test(e){
    // console.log(e)
    this.setData({
      focus:true
    })
  },
  //输入框的高度
  focus(e){
    console.log(e)
    this.setData({
      height:e.detail.height-50
    })
  },
  Blur(e){
    this.setData({
      height:0
    })
  },

  //获取input的内容
  inputChange(e){
    this.setData({
      inputText:e.detail.value,
      
    })
  },
  //点击完成任务
  finish(e){
    
  //  console.log(e.currentTarget.dataset.index)
  //把点击到的todo项拿出来,取反(点击，completed取反),刷新页面
  var item= this.data.todos[e.currentTarget.dataset.index]
  let that =this
  // console.log(item)
  if(item.completed==true){
    
    item.completed=!item.completed
    var leftcount=this.data.leftcount+(item.completed?-1:1)
    var newtodos=this.data.todos.splice(e.currentTarget.dataset.index,1)
    var todos=this.data.todos.unshift(newtodos[0])
    this.setData({
      todos:this.data.todos,
      leftcount:leftcount,
      search:'',
      inputText:''
    })
    wx.setStorage({
      key:'daily',
      data:this.data,
      success(e){}
    })
  }else{
  audio.play()
  item.completed=!item.completed
  //完成(leftcout-1)未完成(leftcount+1)
  var leftcount=this.data.leftcount+(item.completed?-1:1)
  var newtodos=this.data.todos.splice(e.currentTarget.dataset.index,1)
  // console.log(this.data.todos)
  var todos=this.data.todos.push(newtodos[0])
  // console.log(this.data.todos)
  this.setData({
    todos:this.data.todos,
    leftcount:leftcount,
    search:'',
    inputText:''
  })
  wx.setStorage({
    key:'daily',
    data:this.data,
    success(e){}
  })
}
  },
  //删除任务
  clearTodos(e){
    // if (this.data.leftcount==null)
    console.log(e.currentTarget.dataset.index)
    var todos=this.data.todos
    //todos会移除掉index所指向的元素
    var item=todos.splice(e.currentTarget.dataset.index,1)[0]
    // 删除已经完成的数值不变，删除未完成的则减一
    var leftcount=this.data.leftcount-(item.completed? 0:1)
    this.setData({
      todos:todos,
      leftcount:leftcount,
    })
    wx.setStorage({
      key:'daily',
      data:this.data,
      success(e){}
    })
  },

  // 音频输入
  start(){
    wx.showToast({
      title: '正在转换',
      icon:'loading',
      duration:3000,
      // getfocus:false
    })
    this.Distinguish()
    this.setData({
      search:"",
      status:"正在录制"
    })  
    manager.start({
      lang: 'zh_CN',
    })

  },
  end(){
    console.log("结束")
    manager.stop();
    this.setData({
      status:"语音输入"
    }) 
  },
  Distinguish:function(){
    var that=this
    console.log("开始录制")
    //识别结束事件
    manager.onStop = function (res) {
      // console.log(res)
      if (res.result == '') {
        wx.showModal({
          title: '提示',
          content: '听不清楚，请重新说一遍！',
          showCancel: false,
          // success: (res)=> {
          //   this.translate
          // }
        })
        return;
      }  
      // var text=res.result.split()
      // text=text.splice(-1,1)
      // console.log(text)
     var text=res.result
      text=text.slice(0,text.length-1)
      console.log(text)
      that.setData({
        search: text,
        inputText:text
        
      })
    }
  },
  addContent(){
    this.setData({
      addContainerHide:true,
      InputContainerHide:false,
      inputDisable:false,
      unfoldhide:true
    })
  },
  voice(){
    this.setData({
      inputhide:false, 
      height:165,
      btnhide:false,
      inputDisable:true
    })
  },
  keyboard(){
    this.setData({
      inputhide:false,
      getfocus:true
    })
  },
  clean(){
    this.setData({
      inputhide:true,
      InputContainerHide:true,
      addContainerHide:false,
      btnhide:true,
      unfoldhide:false
    })  
  },
  cleaner(){
    this.setData({
      inputhide:true,
      InputContainerHide:true,
      addContainerHide:false,
      btnhide:true,
      unfoldhide:false
    })
  }
})