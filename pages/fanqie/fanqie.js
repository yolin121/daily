var util=require('../../utils/util.js')
const audio=wx.createInnerAudioContext({})
Page({

  data: {
    now: '',
    habbitname:'',
    countdown:'25:00',
    btn:true,
    modalName:"",
    fold:true,
    containColor:'mo',
    colors:['red','yellow','olive','cyan','blue'],
    show:'',
    finishZhuanZhu:false,
    navList:['5分','15分','25分','45分','60分'],
    currentIndexNav:2,
    timeindex:25,//当前选中的专注时间
    musicshow:true,
    word:'不积跬步无以至千里'
  },
  onShow(options){
    this.setData({
      finishZhuanZhu:false
    })
  },

  onLoad: function (options) {
    audio.src="https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/audio/20%20The%20Moment.mp3?sign=b8c7c8fb091c5a5515c8bae203eb2aaa&t=1590249936"
  
  console.log(options.habbitname)
   this.setData({
     habbitname:options.habbitname,
     finishZhuanZhu:false

   })
   wx.request({
     url: 'http://api.tianapi.com/txapi/everyday/index?key=be4a9d7351f83c7159a2e9749be87e47',
     success: res=>{
      if(res.data.code == 200){
      console.log(res.data.newslist[0].note)   
      this.setData({
        word:res.data.newslist[0].note
      })
    }
    }, 
   })
    
  },
  start(){
    var that = this 
    var now=util.formatTime2(new Date())
    that.setData({
     now: now,
     btn:false
    })
    that.countDown()
    wx.showToast({
      title: '屏幕将保持常亮状态',
      icon:'none'
    })
    wx.setKeepScreenOn({
  keepScreenOn: true
})
  },
  onHide: function () {
    wx.setKeepScreenOn({
      keepScreenOn: false
    })

  },
  onUnload: function () {
    wx.setKeepScreenOn({
      keepScreenOn: false
    })

  },
  end(){
    var that=this
    that.setData({
      btn:true
    })
    wx.showModal({
      title: '提示',
      content: '是否结束该项目',
      success (res) {
      if (res.confirm) {
      console.log('用户点击确定')
      that.back()
      } else if (res.cancel) {
      console.log('用户点击取消')
      }
      }
      })
  },
  //点击完成专注
  hideModal(){
    this.setData({
      modalName:''
    })
    let pages = getCurrentPages();//当前页面
    let prevPage = pages[pages.length-2];//上一页面
    prevPage.setData({//直接给上移页面赋值
      finishZhuanZhu:this.data.finishZhuanZhu,
      absorbedTime:this.data.timeindex,
      
    });
    wx.navigateBack({//返回
      delta:1
    })
  },

  //倒计时
  countDown() {
    var that = this
    var starttime = this.data.now
     console.log(starttime)
    var start = new Date(starttime).getTime()
     console.log(start)
    var endTime = start + this.data.timeindex * 60000//开始的时间戳加上需要的时间作为结束的时间
     var now = new Date().getTime(); //现在时间戳
    var allTime = endTime - now//
    var m, s;
    if (allTime > 0) {
      m = Math.floor(allTime / 1000 / 60 % 60);
      s = Math.floor(allTime / 1000 % 60);
      s = s<10?"0"+s:s
      m = m<10?"0"+m:m
      that.setData({
        countdown: m + ":" + s,
      })
      setTimeout(that.countDown, 1000);
    } else {
      console.log('已截止')
      audio.stop()
      that.setData({
        countdown: '00:00',
        modalName:"Image",
        finishZhuanZhu:true
      })
      
    }
  },
  back(){
    wx.navigateBack()
    audio.stop()
  },
  handlefold(){
    this.setData({
      fold:!this.data.fold
    })
  },
  chooseColor(e){
    console.log(e.currentTarget.dataset.index)
    this.setData({
      
      containColor:this.data.colors[e.currentTarget.dataset.index],
      show:'animation: show 2s;'
    })
      
    setTimeout(res=>{
      this.setData({show:''})
    },1000)
  },
  activeNav(e){
    console.log(e.target.dataset.index);
    this.setData({
      currentIndexNav:e.target.dataset.index
    })
    if(e.target.dataset.index==0){
      this.setData({
        countdown:'05:00',
        timeindex:5
      })
    }else if(e.target.dataset.index==1){
      this.setData({
        countdown:'15:00',
        timeindex:15
      })
    }else if(e.target.dataset.index==2){
      this.setData({
        countdown:'25:00',
        timeindex:25
      })
    }else if(e.target.dataset.index==3){
      this.setData({
        countdown:'45:00',
        timeindex:45
      })
    }else if(e.target.dataset.index==4){
      this.setData({
        countdown:'60:00',
        timeindex:60
      })
    }
  },
  playmusic(){
    audio.play()
    
    this.setData({
      musicshow:false
    })
    audio.onEnded(() => {
      this.playmusic();
    })


  },
  pausemusic(){
    audio.stop()
    this.setData({
      musicshow:true
    })
  }
  
})
