// pages/me/me.js
var app=getApp();
const db=wx.cloud.database();
var util=require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    backgroundimg:'./sss.jpg',
    header_img:'../../img/man.png',
    user:'',
    login_show:false,
    text_show:true,
    userOpenid:'',
    suiji:''
  },
  loginshow(){
    // console.log(app.globalData.not_login)
    if(!app.globalData.not_login){
      this.setData({
        login_show:true,
        text_show:false
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.loginshow()
    var u=app.globalData
    this.setData({
      header_img:u.userInfo.avatarUrl,
      user:u.userInfo.nickName,
      
    })
    wx.getStorage({
      key: 'me',
      success:(res)=>{
        console.log(res)
        this.setData({
          backgroundimg:res.data
        })
      }
    })

  },
  userinfo:function(e){
    wx.getUserInfo({
      complete: (res) => {
        // console.log(res)
        this.setData({
          
          header_img:res.userInfo.avatarUrl,
          user:res.userInfo.nickName,
          login_show:true,
          text_show:false,

        
        })
        wx.showToast({
          title: '登陆成功',
        })
       // this.adddatabase()
            //云函数获取用户openid
            wx.cloud.callFunction({
              name:'login',
              success:res=>{
                this.setData({
                  userOpenid:res.result.openid
                })
                app.globalData.userOpenid=res.result.openid;
              }
            })
           
      },
    })
  },
  
  adddatabase(){
   
    //判断如果数据库为空的话就add一个表
  db.collection('demo').where({
    _openid:this.data.userOpenid                                                                                                              
  }).get().then(res=>{
    console.log(res)
    if(res.data==''){
      db.collection('demo').add({
      data:{
        
        
      }
    })

    }
  })
  },
  changeimg(){
    var that=this
    wx.showModal({
      title: '修改背景',
      content: '将从本地上传图片',
      success (res) {
      if (res.confirm) {
    
          that.upimg()
      } else if (res.cancel) {
     
      }
      }
      })
  },
  suijishu(){
    var suiji=Math.floor(Math.random()*100000)
    console.log(suiji)
    this.setData({
      suiji
    })
  },
  upimg(){
    this.suijishu()
    //弹出选择图片对话框
    wx.chooseImage({
      complete: (res) => {
        console.log(res)
        let path=res.tempFilePaths[0]
       wx.showToast({
         title: '图片上传中',
         icon:'loading',
         duration:4000,
         mask:true
       })
        //云存储调用-上传文件方法
        wx.cloud.uploadFile({
          cloudPath:"richang/"+this.data.suiji+".png",
          filePath:path,
          success:(res)=>{
            console.log("图片上传成功",res)
            wx.showToast({
              title: '图片上传成功',
            })
            this.setData({
              backgroundimg:res.fileID
            })
            wx.setStorage({
              key:'me',
              data:this.data.backgroundimg,
              success(e){}
            })
          },
          fail:(res)=>{
            console.log("上传失败",res)
  
          }
        })
  
      },  
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /* 生命周期函数--监听页面卸载 */
  onUnload: function () {

  },

  /*页面相关事件处理函数--监听用户下拉动作 */
  onPullDownRefresh: function () {

  },

  /*页面上拉触底事件的处理函数 */
  onReachBottom: function () {

  },

  /*用户点击右上角分享*/
  onShareAppMessage: function () {

  }
})