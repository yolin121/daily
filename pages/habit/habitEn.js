var app=getApp()
const MONTHS = ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'May.', 'June.', 'July.', 'Aug.', 'Sept.', 'Oct.', 'Nov.', 'Dec.'];
var util=require('../../utils/util.js')
const db=wx.cloud.database();
import * as echarts from '../../ec-canvas/echarts';

function initChart(canvas, width, height, dpr) {
  
 const chart = echarts.init(canvas, null, {
   width: width,
   height: height,
   devicePixelRatio: dpr // new
 });
 canvas.setChart(chart);
let data= [{
 
}]
// 将获取到的数据push进data
for(let i=0;i<app.globalData.globalrenyi.length;i++){
  data.push({
    value:app.globalData.globalrenyi[i].time,
    name:app.globalData.globalrenyi[i].name
  })
}
for(let i=0;i<app.globalData.globalzaochen.length;i++){
  data.push({
    value:app.globalData.globalzaochen[i].time,
    name:app.globalData.globalzaochen[i].name
  })
}
for(let i=0;i<app.globalData.globalzhongwu.length;i++){
  data.push({
    value:app.globalData.globalzhongwu[i].time,
    name:app.globalData.globalzhongwu[i].name
  })
}
for(let i=0;i<app.globalData.globalxiawu.length;i++){
  data.push({
    value:app.globalData.globalxiawu[i].time,
    name:app.globalData.globalxiawu[i].name
  })
}

 var option = {
   backgroundColor: "#ffffff",
   color: ["#37A2DA", "#32C5E9", "#67E0E3", "#91F2DE", "#FFDB5C", "#FF9F7F","#37A2DA", "#32C5E9", "#67E0E3"],
   series: [{
     label: {
       normal: {
         fontSize: 13
       }
     },
     type: 'pie',
     center: ['50%', '50%'],
     radius: ['40%', '60%'],
     data: data
   }]
 };

 chart.setOption(option);
 return chart;
}
Page({
  
  data: {
    tesss:"sss",
    //一个
    yigeimg:'',
    yigejuzi:'',
    //日推
    dayTake:true,
    closeTake:true,
    //未添加任何习惯的时候
    modalName:'',
    //被点击的首页导航
    currentIndexNav:0,
    navList:['我的习惯','数据统计'],
    activeHabbitTime:0,
    show:true,
    //获取时间
    time:"",
    str: MONTHS[new Date().getMonth()],  // 月份字符串
    year: new Date().getFullYear(),      // 年份
    month: new Date().getMonth() + 1,    // 月份
    day: new Date().getDate(),
    demo5_days_style: [],
    habbitIcon:true,
    // detail:"",
    timeLineData: [{
        timeLineDate: '',                                                                      
        timeLineText: '新的开始，新的生活'
      },
    ],
    classifyItem:[
      {id:1,name:'学习',imgSrc:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/habbit.jpeg?sign=a7c16aa07c8874177e76df14ba55bc87&t=1587350807'},
      {id:2,name:'爱好',imgSrc:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/habbit.jpg?sign=3979997fa6701493b493f93d64207a16&t=1587351702'},
      {id:3,name:'健康',imgSrc:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/healthy.jpeg?sign=6d1781bf7b27acb9e24a34215aca7ac0&t=1587352627'},
      {id:4,name:'自定义',imgSrc:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/zidingyi2.jpg?sign=8cffd50f8778551574c015696a6c45ab&t=1587352652'},
     ],
      handleClassifyIndex:-1,
      learnItem:[
        {name:'复习',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/dushu.png?sign=27ec3646c4b4eb92ac3c65ee93874470&t=1588848989',detail:'  复习不仅可防止遗忘、加深理解、熟练技能；而且还可诊断、弥补学习上的知识缺陷，完善自己的知识结构，发展我们的记忆能力和思维能力。'},
        {name:'读书',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/fuxi.jpg?sign=01030a7b74696c0db226f2ad3dace1c5&t=1588849082',detail:'  读书，可以让你觉得有许多的灵感，能陶冶人的情操，给人知识和智慧。一本好书就是一次美妙的心灵之旅。所以，请放下手中的手机，开始阅读吧。'},
        {name:'记英语单词',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/fuxi2.jpg?sign=10a338c5276a9b9bc2dd0e37a76b739d&t=1588849021',detail:'  单词是学习英语的基础，如果说你基础不打好，直接去做和英语有关的事，根本做不好。而背单词是一个非常繁重的任务，它需要大量的精力。如果不制定一个周密的计划，将很难坚持。'},
        {name:'写日记',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/riji.jpg?sign=5ae8635f72b19fbe402d2540cf1cacbc&t=1588849065',detail:'  只要写一阵子日记，再回去看看，你就会发现：如果不记下来，你会完全忘掉自己曾有过那么牛逼的想法和绝妙的体验和富有美感的表达，而你根本不想忘掉他们。所以你会不断地记下去。   --来源:知乎'},
        {name:'专注',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/zhuanzhu.jpeg?sign=68b082ce4ae5a9e6a596207e50197fd6&t=1590072998',detail:'学习效果=有效学习时间*学习状态*学习方法.  这里推荐番茄工作法，25分钟+5分钟的结合练习，在高专注力的间隔间休息一回，会像按了重开机键一样，重新进入高度专注状态'},
        {name:'学习一门语言',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/xuexiyuyan.jpg?sign=3251d0a7bccba2c98992fb18672da6ee&t=1590073050',detail:'学语言真的是可以让你更了解一门文化的，当然它也是一门工具，可以认为是谋生的工具，也可以认为是去探索另外一个世界的工具。'},
        {name:'制作思维导图',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/mind.jpg?sign=50fef030d91bea6600222c7afc81beb3&t=1591522841',detail:'思维导图又叫心智导图，是表达发散性思维的有效图形思维工具 ，它简单却又很有效，是一种革命性的思维工具。思维导图运用图文并重的技巧，把各级主题的关系用相互隶属与相关的层级图表现出来，把主题关键词与图像、颜色等建立记忆链接。'}
      ],
      hobbyItem:[
        {name:'学习乐器',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/yueqi.jpg?sign=4c6a45b18a8df0bcb758e826238be6f4&t=1588851313',detail:"美国西北大学的科学家研究了高中生学习音乐，结果发现，学习乐器可以提高他们的对声音的反应能力和语言技能。学乐器的学生更擅长感知声音的细节，大脑负责声音的脑区更加成熟，并且语言能力也提高得更多。"},
        {name:'养花',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/hua.jpg?sign=0e969485e61de2bf87bda897be26ced2&t=1588851337',detail:'养花可以感受到时间的步伐，让我们在忙碌的工作中发现生活乐趣，让我们在喧嚣里找寻内心宁静。花开的时候或许你也会收获一份大大的惊喜呢！'},
        {name:'理财',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/travl.jpg?sign=4a59f1b38fdfa372cb27570b8ce20b00&t=1588851323',detail:'投资理财帮助我们清楚自己的财务状况，让我们的支出更加合理，通过有针对性的持续调整，就可以减少浪费，节省更多的钱用于理财或者花在其他需要的地方。'},
        {name:'画画',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/huahua.jpeg?sign=ed1d7d0dceae7e620ae0cfecea2d3065&t=1588851351',detail:'人的一生分为很多阶段，少年时，可以以画言志；青壮年时，生活压力巨大，可以以画解压；等到人过中年，孩子都已成家立业，自己也临近退休，内心难免会有孤独之感，此时可以画画消遣。画画使人始终心有所寄，志有所托。--来源：知乎'},
        {name:'烹饪',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/zuofan.jpg?sign=744ccdde16c3d63fb3b4164187451a98&t=1590073118',detail:'烹饪是一种挑战，在美食出锅之前你并不知道是什么味道，不断尝试着如何让它变得美味。其次能锻炼动手能力，培养生活情趣，并且健康，甚至会省钱。'},
        {name:'摄影',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/sheying.jpeg?sign=3c1286bbcea30cb897f7b80511faa874&t=1590073158',detail:'摄影对我们的生活产生了非凡影响。其从一开始就改变了人们审视自身的方法，几乎所有领域都因摄影而发生了革命性转变。这些领域包括社会者、科学．艺术、政治、历史等等。摄影不仅改变了人们看世界的角度、维度，而且正在重新塑造我们的世界。'},
        {name:'滑板',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/huaban11.jpg?sign=0ae5d79053e20d13702504203ed54e31&t=1590073181',detail:'玩滑板可以培养一个人的恒心，毅力，最主要的是能培养百折不挠的精神，无数次的失败换来一次次的成功，内心取得的成就感是他人无法想象的，滑板的魅力就在于可以不断的挑战自我。'}
      ],
      healthyItem:[
        {name:'早睡',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/shuimian.jpeg?sign=21ec9cf7c41975435b8cb2b48ed90f24&t=1588851700',detail:'医学上，早睡的好处有：1、保持体型；2、有益心脏；3、减缓压力；4、提高记忆力；5、免受疾病困扰；6、有时间吃早餐；7、有乐观的行动；8、有助于新陈代谢；9、减少得癌症的几率；10、养精蓄锐'},
        {name:'冥想',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/mingxiang2.jpg?sign=94e5b3f47ecacf3410d680cecc2859e6&t=1588851968',detail:'冥想的目的是为了集中精神、放松心灵，最终达到对自我意识更清晰的掌控和内心深处的平静。你可以在任何地方、任何时间进行冥想，无论周围多么喧哗，你都能获得平和'},
        {name:'运动',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/yundong2.jpg?sign=dc151ce6412dafc70e869500da16de87&t=1588852112',detail:'如果说运动是一种药的话，这种药的药效好得简直让人难以置信。运动不仅能让我们保持健康和延长寿命，而且能让我们变得更加聪明和快乐。它能增强记忆力、提高反应速度、提高注意力和缓解抑郁症状，甚至还能延缓阿尔兹海默病和帕金森病等神经退行性疾病的病程进展。'},
        {name:'散步',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/sanbu.jpg?sign=b07013eba997affe7f98b3e81380130e&t=1588851746',detail:'散步作为一种简便的有氧运动对身体非常有益处，例如可以改善人体的新陈代谢和降低血糖，可以一定程度增强心脏功能，减缓心肌梗塞等等'},
        {name:'拉伸',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/shenzhan.jpg?sign=114f489ce1a00d82f1313a0fd4c75271&t=1590073277',detail:'拉伸可以健身锻炼，美体。经常做拉伸可以帮助我们在开展其他健身锻炼时的体态更完美。也能促进血液循环，精力充沛。上班族如果工作需要天天坐在办公室，那么在下午时做做拉伸动作可以起到比喝咖啡喝茶更好的提神作用。'},
        {name:'午休',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/wuxiu.jpg?sign=87d1c40571f3d14ab77d0a33eced6381&t=1590073234',detail:'午休有助于提高效率和创造力。帮助大脑在执行诸如集中、决策，解决问题和交流等众多任务之后恢复，从而避免精神疲劳。为了保证下午的工作效率以及学习效率，这个午休就起着很关键的作用。'},
        {name:'喝水',url:'https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/water.jpg?sign=8a1e9b6d329b5f3292ab4801df380358&t=1591521931',detail:'当血压经常过高时，喝水会降低血压。身体的水分越多，血管就越不僵硬，因此，你的血压就越不容易升高。保持身体水分可以降低动脉高血压和血压不平衡带来的风险，如：心脏病、中风、心脏衰竭。因此，喝水几乎是对我们自己和我们的健康的一种责任。'}
      ],
      handleLearnIndex:-1,
      inputvalue:'', //输入框显示值
      inputValue:'',
      lianxutiansh:0, //初始连续时间
      //任意时段
      renyishiduan:[],
      //早晨习惯
      zaochenxiguan:[],
      //中午习惯
      zhongwuxiguan:[],
      //下午习惯
      xiawuxiguan:[],
      //选择的习惯名称
      choosedHabbitName:'',
      //选择的习惯图标
      choosedHabbitIconName:'',
      //选择的习惯时间段
      choosedHabbitTimeIndex:0,
      //选择的习惯图标颜色
      chooseHabbitColor:'',
      chooseColorIndex:-1,
      chooseIconIndex:-1,
      
      iconColors:['red','orange','yellow','olive','green','cyan','blue','purple','mauve','pink','brown','grey','black','gray'],
      habbitTimeList:['任意时段','早晨习惯','中午习惯','下午习惯'],
      icon: [{ name: 'writefill', isShow: true },{ name: 'communityfill', isShow: true },  { name: 'dianhua', isShow: true }, { name: 'read', isShow: true },{ name: 'musicfill', isShow: true },{ name: 'coin', isShow: true },  { name: 'evaluate_fill', isShow: true }, { name: 'skin', isShow: true }, { name: 'colorlens', isShow: true },{ name: 'appreciate', isShow: true },  { name: 'edit', isShow: true }, { name: 'emoji', isShow: true }, { name: 'favorfill', isShow: true },  { name: 'explorefill', isShow: true },  { name: 'search', isShow: true }, { name: 'taxi', isShow: true }, { name: 'timefill', isShow: true },  { name: 'warnfill', isShow: true }, { name: 'camerafill', isShow: true }, { name: 'commentfill', isShow: true },  { name: 'likefill', isShow: true }, { name: 'fork', isShow: true },
         { name: 'deliver', isShow: true }, { name: 'pay', isShow: true }, { name: 'send', isShow: true }, { name: 'discover', isShow: true }, { name: 'list', isShow: true },  { name: 'settings', isShow: true }, { name: 'questionfill', isShow: true }, { name: 'shopfill', isShow: true }, { name: 'form', isShow: true }, { name: 'pic', isShow: true }, { name: 'filter', isShow: true }, { name: 'footprint', isShow: true },  { name: 'deletefill', isShow: true }, { name: 'refund', isShow: true }, { name: 'cart', isShow: true },
        { name: 'qrcode', isShow: true }, { name: 'remind', isShow: true }, { name: 'delete', isShow: true }, { name: 'profile', isShow: true }, { name: 'home', isShow: true }, { name: 'cartfill', isShow: true }, { name: 'discoverfill', isShow: true }, { name: 'homefill', isShow: true }, { name: 'message', isShow: true }, { name: 'addressbook', isShow: true }, { name: 'link', isShow: true }, { name: 'vip', isShow: true }, { name: 'weibo', isShow: true }, { name: 'activity', isShow: true }, { name: 'friendaddfill', isShow: true }, { name: 'friendadd', isShow: true }, { name: 'friendfamous', isShow: true }, { name: 'friend', isShow: true }, { name: 'goods', isShow: true }, { name: 'selection', isShow: true }, { name: 'explore', isShow: true }, { name: 'present', isShow: true },  { name: 'squarecheck', isShow: true }, { name: 'round', isShow: true }, 
       { name: 'game', isShow: true }, { name: 'redpacket', isShow: true }, { name: 'selectionfill', isShow: true }, { name: 'similar', isShow: true }, { name: 'appreciatefill', isShow: true }, { name: 'rechargefill', isShow: true }, { name: 'vipcard', isShow: true }, { name: 'voice', isShow: true }, { name: 'voicefill', isShow: true }, { name: 'friendfavor', isShow: true }, { name: 'wifi', isShow: true }, { name: 'share', isShow: true }, { name: 'wefill', isShow: true }, { name: 'lightauto', isShow: true }, { name: 'lightforbid', isShow: true }, { name: 'lightfill', isShow: true }, { name: 'camerarotate', isShow: true }, { name: 'light', isShow: true }, { name: 'barcode', isShow: true }, 
        { name: 'flashlightclose', isShow: true }, { name: 'flashlightopen', isShow: true }, { name: 'searchlist', isShow: true }, { name: 'service', isShow: true }, { name: 'sort', isShow: true }, { name: 'mobile', isShow: true }, { name: 'mobilefill', isShow: true }, { name: 'copy', isShow: true }, { name: 'countdownfill', isShow: true }, { name: 'countdown', isShow: true }, { name: 'noticefill', isShow: true }, { name: 'notice', isShow: true }, { name: 'upstagefill', isShow: true }, { name: 'upstage', isShow: true }, { name: 'babyfill', isShow: true }, { name: 'baby', isShow: true }, { name: 'brandfill', isShow: true }, { name: 'brand', isShow: true }, { name: 'choicenessfill', isShow: true }, { name: 'choiceness', isShow: true }, { name: 'clothesfill', isShow: true }, { name: 'clothes', isShow: true }, { name: 'creativefill', isShow: true }, { name: 'creative', isShow: true }, { name: 'female', isShow: true }, { name: 'keyboard', isShow: true }, { name: 'male', isShow: true }, 
        { name: 'newfill', isShow: true }, { name: 'new', isShow: true },  { name: 'rankfill', isShow: true }, { name: 'bad', isShow: true }, { name: 'cameraadd', isShow: true }, { name: 'focus', isShow: true }, { name: 'friendfill', isShow: true }, { name: 'cameraaddfill', isShow: true }, { name: 'apps', isShow: true }, { name: 'paintfill', isShow: true }, { name: 'paint', isShow: true }, { name: 'picfill', isShow: true },  { name: 'markfill', isShow: true }, { name: 'mark', isShow: true }, { name: 'presentfill', isShow: true },{ name: 'album', isShow: true }, { name: 'peoplefill', isShow: true },  { name: 'servicefill', isShow: true }, { name: 'repair', isShow: true }, { name: 'file', isShow: true }, { name: 'repairfill', isShow: true }, { name: 'taoxiaopu', isShow: true },
         { name: 'weixin', isShow: true },  { name: 'commandfill', isShow: true },   { name: 'tagfill', isShow: true }, { name: 'group', isShow: true }, { name: 'all', isShow: true }, { name: 'backdelete', isShow: true }, { name: 'hotfill', isShow: true }, { name: 'hot', isShow: true }, { name: 'post', isShow: true }, { name: 'radiobox', isShow: true }, { name: 'upload', isShow: true },{ name: 'write', isShow: true }, { name: 'radioboxfill', isShow: true }, { name: 'punch', isShow: true }, { name: 'shake', isShow: true },  { name: 'safe', isShow: true }, { name: 'activityfill', isShow: true }, { name: 'crownfill', isShow: true }, { name: 'goodsfill', isShow: true }, { name: 'messagefill', isShow: true }, { name: 'profilefill', isShow: true }, { name: 'sound', isShow: true }, { name: 'sponsorfill', isShow: true },
          { name: 'sponsor', isShow: true }, { name: 'upblock', isShow: true }, { name: 'weblock', isShow: true }, { name: 'weunblock', isShow: true }, { name: 'my', isShow: true }, { name: 'myfill', isShow: true }, { name: 'emojifill', isShow: true }, { name: 'emojiflashfill', isShow: true }, { name: 'flashbuyfill', isShow: true }, { name: 'text', isShow: true }, { name: 'goodsfavor', isShow: true }, { name: 'musicforbidfill', isShow: true }, { name: 'card', isShow: true },   { name: 'font', isShow: true }, { name: 'recordfill', isShow: true },  { name: 'cardboardfill', isShow: true },  { name: 'formfill', isShow: true },  { name: 'cardboardforbid', isShow: true }, { name: 'circlefill', isShow: true }, { name: 'circle', isShow: true }, { name: 'attentionforbidfill', isShow: true }, { name: 'attentionfavorfill', isShow: true }, 
         { name: 'mail', isShow: true }, { name: 'peoplelist', isShow: true }, { name: 'goodsnewfill', isShow: true }, { name: 'goodsnew', isShow: true }, { name: 'medalfill', isShow: true }, { name: 'newsfill', isShow: true }, { name: 'newshotfill', isShow: true }, { name: 'newshot', isShow: true }, { name: 'news', isShow: true }, { name: 'videofill', isShow: true },  { name: 'exit', isShow: true },  { name: 'moneybagfill', isShow: true }, { name: 'redpacket_fill', isShow: true }, { name: 'subscription', isShow: true },  { name: 'github', isShow: true }, { name: 'global', isShow: true }, { name: 'settingsfill', isShow: true },  { name: 'expressman', isShow: true }, { name: 'group_fill', isShow: true }, { name: 'play_forward_fill', isShow: true }, { name: 'deliver_fill', isShow: true }, { name: 'notice_forbid_fill', isShow: true },
            { name: 'pick', isShow: true }, { name: 'wenzi', isShow: true },  { name: 'icon', isShow: true },  { name: 'btn', isShow: true }],

      //日历
      //demo5_days_style: [],
      myHabbitDetail:false, //显示习惯详细内容
      HabbitDetailIndex:{},  //点击的我的习惯中的某一个索引
     // HabbitDetailZaoIndex:-1  //点击早晨我的习惯中的某一个索引
     dangqianjieduan:{},
     habbitname:"",  //存要写入数据统计时间轴的名称
     Detail:[],  //存要显示的detail来自哪个分类
     alreadyClock:false,  //判断是否为当日第二次打卡
     shijianming:[],
     userOpenid:'',
     _id:'',
     alreadyClockIndex:-1,
     chudi:false,   //触底
     pagenum: 1,

    //  数据统计
    dakacishu:0,
    countdays:0,
    totalAbsorbedTime:0,
    zhuanzhucishu:0,
    savaSuccess:false,
    ec: {
 
      onInit: initChart,

    }

  },

dayClick(e){console.log(e.detail.day)
  if(this.data.day==e.detail.day){
    wx.showToast({
      title: '业精于勤，荒于嬉；行成于思，毁于随。(韩愈）',
      icon:'none',
      duration:3500
    })
  }else if(this.data.day>e.detail.day){
    wx.showToast({
      title: '人生天地之间，若白驹过隙，忽然而已。（庄子）',
      icon:'none',
      duration:3500
    })
  }else{
    wx.showToast({
      title: '天可补，海可填，南山可移。日月既往，不可复追。（曾国藩）',
      icon:'none',
      duration:3500
    })
  }
},
onReady: function () {
    this.ecComponent = this.selectComponent('#mychart-dom-pie');
    setTimeout(res=>{
     
    this.onLoad()
  
},500)

},
 onShow(options){
  
  console.log("完成专注"+this.data.finishZhuanZhu)
  console.log("专注时常"+this.data.absorbedTime)
  if(this.data.finishZhuanZhu==true){   //fanqie页面传过来的
    var totalAbsorbedTime=this.data.totalAbsorbedTime+this.data.absorbedTime
    this.setData({
      totalAbsorbedTime,
      absorbedTime:0,
      zhuanzhucishu:this.data.zhuanzhucishu+1,
      finishZhuanZhu:false
    })
    this.finshToday()
  }else{
    console.log("fnishZhuanZhu为false")
  }
 
 this.dakatianshu()
 },
  onLoad: function (options) {
    // console.log(this.data.ec)
    // console.log(this.data.ec.onInit)
    // console.log(this.data.ec.option)
    console.log("onload页面加载")
    console.log(app.globalData.userOpenid)
    this.setData({
      userOpenid:app.globalData.userOpenid
    })
        wx.request({
      url: 'https://api.tianapi.com/txapi/one/index?key=be4a9d7351f83c7159a2e9749be87e47', 
      success: res=>{
        if(res.data.code == 200){
        console.log(res.data.newslist[0])   
        this.setData({
          yigeimg:res.data.newslist[0].imgurl,
          yigejuzi:res.data.newslist[0].word
        })
      }
      }, 
    })

    //判断如果数据库为空的话就add一个表
  db.collection('demo').where({
    _openid:this.data.userOpenid                                                                                                              
  }).get().then(res=>{
    console.log(res)
    if(res.data==''){
      db.collection('demo').add({
      data:{
        date:util.myformatTime(new Date()),
        renyishiduan:this.data.renyishiduan,
        zaochenxiguan:this.data.zaochenxiguan,
        zhongwuxiguan:this.data.zhongwuxiguan,
        xiawuxiguan:this.data.xiawuxiguan,
        timeLineData:this.data.timeLineData,
        demo5_days_style:this.data.demo5_days_style,
        totalAbsorbedTime:this.data.totalAbsorbedTime,
        zhuanzhucishu:this.data.zhuanzhucishu
      }
    })

    }
  })
    //云函数获取用户openid
    // wx.cloud.callFunction({
    //   name:'login',
    //   success:res=>{
    //     console.log("用户openid"+res.result.openid)
    //     this.setData({
    //       userOpenid:res.result.openid
    //     })
    //   }
    // })

    
    wx.showToast({
      title: '正在更新数据',
      icon:'loading',
      duration:1000
    })

    




  // 查看当前数组demo5_days_style
  //console.log(this.data.demo5_days_style)
  const days_count = new Date(this.data.year, this.data.month, 0).getDate();
  console.log("当月天数"+days_count)//当月的天数
  let demo5_days_style = new Array;
  for (let i = 1; i <= days_count; i++) {
      const date = new Date(this.data.year, this.data.month - 1, i);
      if (date.getDay() == 0) {
          demo5_days_style.push({
              //month: 'current', day: i, color: '#f488cd'
          })
      }else {
          demo5_days_style.push({
             // month: 'current', day: i, color: '#a18ada'
          });
      }
  }
  this.setData({
      demo5_days_style
  })


  this.readdatabase()

  //如果日期为1号，并且全局变量cleardemo5为true,将数据库的demo5清除，再将cleardemo5设置为false，这样当天只会执行一次
  //如果日期为2号，将全局变量设为true，
  if(this.data.day==3&&app.globalData.cleardemo5){
    //数据库
    console.log("数据库的demo5清除")
  db.collection('demo').doc(this.data._id).update({
    data:{
      demo5_days_style:[],
    },
    success:res=>{
      console.log("数据更新成功"+res)
    },
    fail:err=>{
      wx.showToast({
        title: '数据异常',
        icon:'none'
      })
    }
  })

    app.globalData.cleardemo5=false



  }else if(this.data.day==4){
    app.globalData.cleardemo5=true
  }

   this.dakatianshu()
   //this.bingtu()
   //this.init()

   
  },
  //读取数据库
  readdatabase(){
     //读取用户保存的数据
     db.collection('demo').where({
      _openid:this.data.userOpenid
    }).get().then(res=>{
      console.log(res)
      var now=util.myformatTime(new Date())
      var mydate=res.data[0].date
      console.log("当前时间"+now)
      console.log(mydate)
      //判断数据库中的打卡时间与现在时间是否统一
       if(mydate==now){
        this.setData({
          _id:res.data[0]._id,
          demo5_days_style:res.data[0].demo5_days_style,
          renyishiduan:res.data[0].renyishiduan,
          timeLineData:res.data[0].timeLineData,
          zhongwuxiguan:res.data[0].zhongwuxiguan,
          xiawuxiguan:res.data[0].xiawuxiguan,
          zaochenxiguan:res.data[0].zaochenxiguan,
          totalAbsorbedTime:res.data[0].totalAbsorbedTime,
          zhuanzhucishu:res.data[0].zhuanzhucishu
        })
       }else{
         //时间不一样的话
         console.log("我们不一样不一样")
         let renyishiduan=res.data[0].renyishiduan;
         for(var i=0;i<renyishiduan.length;i++){
           renyishiduan[i].alreadyClock=false 
         }
         let zaochenxiguan=res.data[0].zaochenxiguan;
         for(var i=0;i<zaochenxiguan.length;i++){
           zaochenxiguan[i].alreadyClock=false
         }
         let zhongwuxiguan=res.data[0].zhongwuxiguan;
         for(var i=0;i<zhongwuxiguan.length;i++){
           zhongwuxiguan[i].alreadyClock=false
         }
         let xiawuxiguan=res.data[0].xiawuxiguan;
         for(var i=0;i<xiawuxiguan.length;i++){
           xiawuxiguan[i].alreadyClock=false
         }
         this.setData({
           renyishiduan,
           zaochenxiguan,
           zhongwuxiguan,
           xiawuxiguan,
           _id:res.data[0]._id,
           demo5_days_style:res.data[0].demo5_days_style,
           timeLineData:res.data[0].timeLineData,
           totalAbsorbedTime:res.data[0].totalAbsorbedTime,
           zhuanzhucishu:res.data[0].zhuanzhucishu

         })
       }
    })
  },

  //计算打卡的总天数和总打卡次数
  dakatianshu(){
    var demo5_days_style=this.data.demo5_days_style
    var countdays=0
    for(let i=0;i<demo5_days_style.length-1;i++){
     if(demo5_days_style[i].day!=demo5_days_style[i+1].day) {
        countdays=countdays+1
     }
    }
    console.log("总打卡天数"+countdays)
    this.setData({
      countdays,
      dakacishu:this.data.timeLineData.length-1,
   
    })
  },
  navgite(){
    this.setData({
      closeTake:false
    })
    setTimeout(()=>{
      this.setData({
        dayTake:false
      })
    },1300)
    setTimeout(()=>{
      if(!this.data.renyishiduan[0]&&!this.data.zaochenxiguan[0]&&!this.data.zhongwuxiguan[0]&&!this.data.xiawuxiguan[0]){
        wx.showToast({
          title: '当前习惯为空，点击右下角添加吧！',
          icon:'none',
          duration:4000
        })
        this.setData({
          modalName:"Image"

        })
    }
    },2000)
    this.dakatianshu()
  },
  //隐藏提示对话框（无习惯）
  hideModal(){
    this.setData({
      modalName:""
    })

  },
  //点击导航页
  activeNav(e){
    console.log(e.target.dataset.index);
    this.setData({
      currentIndexNav:e.target.dataset.index
    })
  },
  
  //点击添加习惯
  addHabbit:function(){
    this.setData({
      habbitIcon:!this.data.habbitIcon
    })

  },

  //点击叉叉
  closeAddHabbit(){
    this.setData({
      habbitIcon:!this.data.habbitIcon,
      handleClassifyIndex:-1,
      handleLearnIndex:-1,
      // myHabbitDetail:false
    })
  },
  //打卡那一块内容
  handleClick(e){
    
    var timeLineData=this.data.timeLineData
    timeLineData.unshift({timeLineDate: this.data.time,
    timeLineText: this.data.habbitname})
   // console.log(timeLineData)

 
    const days_count = new Date(this.data.year, this.data.month, 0).getDate();
    let demo5_days_style = this.data.demo5_days_style;
    for (let i = 1; i <= days_count; i++) {
        const date = new Date(this.data.year, this.data.month - 1, i);
         if (i == this.data.day) {
            demo5_days_style.push({
              month: 'current', day: i, color: 'white', background: '#aad4f5'
            })
        } 
        else {
          // demo5_days_style.push({
          //     month: 'current', day: i, color: '#a18ada'
          // })
      }
    }
    console.log(this.data.demo5_days_style)
    this.setData({
      timeLineData,
      demo5_days_style,
     
    })
  },
  //获取当前时间
  getTime(){
     // 获取util时间
     var TIME=util.formatTime(new Date());
     console.log(TIME)
     this.setData({
       time:TIME
     }) 

  },
  //点击某一分类
  handleClassify(e){
    console.log(e.target.dataset.index)
    let index=e.target.dataset.index
    if(index==0){
      this.setData({Detail:this.data.learnItem})
    }else if(index==1){
      this.setData({Detail:this.data.hobbyItem})
    }else if(index==2){
      this.setData({Detail:this.data.healthyItem})
    }
    let that=this
    if(index==3){
      that.setData({
        handleLearnIndex:0,
        inputValue:'',
        inputvalue:''
      })
    }
    this.setData({
      handleClassifyIndex:e.target.dataset.index
    })
  },
  //点击学习分类中的某一项
  handleLearnIndex(e){
    console.log(e.target.dataset.index)
    let index=e.target.dataset.index 
    let name=this.data.learnItem[index].name //点击的学习里的名字
    console.log(name) //点击的习惯名称
    this.setData({
      handleLearnIndex:e.target.dataset.index,
      inputvalue:name,
      choosedHabbitName:name  //把点击的习惯名称存起来
    })
  },
  //点击爱好分类中的某一项
  handlehobbyIndex(e){
    let index=e.target.dataset.index 
    let name=this.data.hobbyItem[index].name 
    console.log(name) //点击的习惯名称
    this.setData({
      handleLearnIndex:e.target.dataset.index,
      inputvalue:name,
      choosedHabbitName:name 
    })

  },
  //点击健康分类中的某一项
  handleHealthyIndex(e){
    let index=e.target.dataset.index 
    let name=this.data.healthyItem[index].name 
    console.log(name) //点击的习惯名称
    this.setData({
      handleLearnIndex:e.target.dataset.index,
      inputvalue:name,
      choosedHabbitName:name 
    })

  },
  //输入框值
  inputdetail(e){
    console.log(e.detail.value)
    this.setData({
      inputValue:e.detail.value,
      inputvalue:e.detail.value,
      choosedHabbitName:e.detail.value
    })
  },
   //点击的图标索引
   clickIconIndex(e){
    console.log(e.target.dataset.index) //点击图标的索引
    console.log(this.data.icon[e.target.dataset.index].name) //点击图标的名称
    let iconname=this.data.icon[e.target.dataset.index].name
    this.setData({
      choosedHabbitIconName: iconname,
      chooseIconIndex:e.target.dataset.index
    })
  },
  //点击的时间索引
  activeHabbitTime(e){
    // console.log(e.target.dataset.index)
    // console.log(this.data.habbitTimeList[e.target.dataset.index])
    this.setData({
      activeHabbitTime:e.target.dataset.index
    })
  },

  //点击保存按钮：添加到我的习惯中
  addMyHabbit(){
    if(this.data.choosedHabbitName==''||this.data.choosedHabbitIconName==''){
      wx.showToast({
        title: '内容不完整',
        icon:'none'
      })
      return
    }
    //如果时段索引是0,添加给renyishiduan数组
    if(this.data.activeHabbitTime==0){
    let renyishiduan=this.data.renyishiduan
    renyishiduan.push({icon:this.data.choosedHabbitIconName,iconColor:this.data.chooseHabbitColor,name:this.data.choosedHabbitName,time:0,alreadyClock:false})
    this.setData({
      renyishiduan,
    }) 
  }else if(this.data.activeHabbitTime==1){
    let zaochenxiguan=this.data.zaochenxiguan
    zaochenxiguan.push({icon:this.data.choosedHabbitIconName,iconColor:this.data.chooseHabbitColor,name:this.data.choosedHabbitName,time:0,alreadyClock:false})
    this.setData({
      zaochenxiguan,
    }) 
  }else if(this.data.activeHabbitTime==2){
    let zhongwuxiguan=this.data.zhongwuxiguan
    zhongwuxiguan.push({icon:this.data.choosedHabbitIconName,iconColor:this.data.chooseHabbitColor,name:this.data.choosedHabbitName,time:0,alreadyClock:false})
    this.setData({
      zhongwuxiguan,
    }) 
  }else{
    let xiawuxiguan=this.data.xiawuxiguan
    xiawuxiguan.push({icon:this.data.choosedHabbitIconName,iconColor:this.data.chooseHabbitColor,name:this.data.choosedHabbitName,time:0,alreadyClock:false})
    this.setData({
      xiawuxiguan,
    })
  }
  //点击保存按钮就会做的事
  this.setData({
    habbitIcon:!this.data.habbitIcon,
      handleClassifyIndex:-1,
      handleLearnIndex:-1,
      inputvalue:'',
      inputValue:'',
      choosedHabbitName:'',
      chooseIconIndex:-1,
      chooseColorIndex:-1,
      savaSuccess:true
  })
  // wx.setStorage({
  //   key:'habbit',
  //   data:this.data,
  //   success(e){}
  // })
  db.collection('demo').doc(this.data._id).update({
    data:{
      date:util.myformatTime(new Date()),
      renyishiduan:this.data.renyishiduan,
      zaochenxiguan:this.data.zaochenxiguan,
      zhongwuxiguan:this.data.zhongwuxiguan,
      xiawuxiguan:this.data.xiawuxiguan,
      
    },
    success:res=>{
      console.log("更新数据库成功"+res)
    }
  })
  app.globalData.globalrenyi=this.data.renyishiduan
  app.globalData.globalzaochen=this.data.zaochenxiguan
  app.globalData.globalzhongwu=this.data.zhongwuxiguan
  app.globalData.globalxiawu=this.data.xiawuxiguan
  wx.switchTab({
    url: './habitEn',
  })

  },
  //点击颜色
  chooseColor(e){
    console.log(this.data.iconColors[e.target.dataset.index])
    this.setData({
      chooseHabbitColor:this.data.iconColors[e.target.dataset.index],
      chooseColorIndex:e.target.dataset.index
    })
  },

  //点击任意时段中我的习惯时
  handleClickRenyi(e){
    
    let time=this.data.renyishiduan[e.target.dataset.renindex].time //获取到连续的时间
    if(time<3){
      this.quanlibaofa()
    }else if(time<7){
      this.yizhipiruan()
    }else if(time<21){
      this.xiedaimamu()
    }else{
      this.pingwen()
    }
    this.setData({
       
      alreadyClockIndex:e.target.dataset.renindex,//判断重复点击的index
      shijianming:this.data.renyishiduan,  //点击的时间段
      habbitname:this.data.renyishiduan[e.target.dataset.renindex].name,
      lianxutiansh:time,
      myHabbitDetail:true,
      HabbitDetailIndex:e.target.dataset  //拿到点击的索引
    })
  },
  //点击早晨时段中我的习惯时
  handleClickZaochen(e){
    let time=this.data.zaochenxiguan[e.target.dataset.zaoindex].time //获取到连续的时间
    if(time<3){
      this.quanlibaofa()
    }else if(time<7){
      this.yizhipiruan()
    }else if(time<21){
      this.xiedaimamu()
    }else{
      this.pingwen()
    }
    this.setData({
      alreadyClockIndex:e.target.dataset.zaoindex,//判断重复点击的index
      shijianming:this.data.zaochenxiguan,  //点击的时间段
      habbitname:this.data.zaochenxiguan[e.target.dataset.zaoindex].name,
      lianxutiansh:time,
      myHabbitDetail:true,
      HabbitDetailIndex:e.target.dataset
    })
  },
  //点击中午时段中我的习惯时
  handleClickZhongwu(e){
    let time=this.data.zhongwuxiguan[e.target.dataset.zhongindex].time //获取到连续的时间
    if(time<3){
      this.quanlibaofa()
    }else if(time<7){
      this.yizhipiruan()
    }else if(time<21){
      this.xiedaimamu()
    }else{
      this.pingwen()
    }
    this.setData({
      alreadyClockIndex:e.target.dataset.zhongindex,//判断重复点击的index
      shijianming:this.data.zhongwuxiguan,  //点击的时间段
      habbitname:this.data.zhongwuxiguan[e.target.dataset.zhongindex].name,

      lianxutiansh:time,
      myHabbitDetail:true,
      HabbitDetailIndex:e.target.dataset
    })
  },
  //点击下午时段中我的习惯时
  handleClickXiawu(e){
    let time=this.data.xiawuxiguan[e.target.dataset.xiaindex].time //获取到连续的时间
    if(time<3){
      console.log("全力爆发期")
      this.quanlibaofa()
    }else if(time<7){
      this.yizhipiruan()
    }else if(time<21){
      this.xiedaimamu()
    }else{
      this.pingwen()
    }
    this.setData({
      alreadyClockIndex:e.target.dataset.xiaindex,//判断重复点击的index
      shijianming:this.data.xiawuxiguan,  //点击的时间段
      habbitname:this.data.xiawuxiguan[e.target.dataset.xiaindex].name,
      lianxutiansh:time,
      myHabbitDetail:true,
      HabbitDetailIndex:e.target.dataset
    })
  },
  closeDetail(){
    this.setData({
      myHabbitDetail:false
    })
  },
  finshToday(){
    //判断是否为当日第二次打卡
   
    wx.showToast({
      title: '打卡成功！',
    })
    //判断点击的是那个时间段的数组被点击了
   if(this.data.HabbitDetailIndex.renindex>=0){
     let renyishiduan=this.data.renyishiduan
     let index=this.data.HabbitDetailIndex.renindex //获取到点击的索引
     var time=renyishiduan[index].time+1
     renyishiduan[index].time=time
     renyishiduan[index].alreadyClock=true
     this.setData({
       renyishiduan,
      
     })
   }else if(this.data.HabbitDetailIndex.zaoindex>=0){
     console.log("2222222")
     let zaochenxiguan=this.data.zaochenxiguan
     let index=this.data.HabbitDetailIndex.zaoindex
     var time=zaochenxiguan[index].time+1
     zaochenxiguan[index].time=time
     zaochenxiguan[index].alreadyClock=true
     this.setData({
       zaochenxiguan
     })
   }else if(this.data.HabbitDetailIndex.zhongindex>=0){
    console.log("3333333")
    let zhongwuxiguan=this.data.zhongwuxiguan
    let index=this.data.HabbitDetailIndex.zhongindex
    var time=zhongwuxiguan[index].time+1
    zhongwuxiguan[index].time=time
    zhongwuxiguan[index].alreadyClock=true
    this.setData({
      zhongwuxiguan
    })
  }else if(this.data.HabbitDetailIndex.xiaindex>=0){
    console.log("4444444")
    let xiawuxiguan=this.data.xiawuxiguan
    let index=this.data.HabbitDetailIndex.xiaindex
    var time=xiawuxiguan[index].time+1
    xiawuxiguan[index].time=time
    xiawuxiguan[index].alreadyClock=true
    this.setData({
      xiawuxiguan
    })
  }
  this.getTime()
  this.handleClick()
  this.setData({
    myHabbitDetail:false
  })
  //数据库
  db.collection('demo').doc(this.data._id).update({
    data:{
      lianxutiansh:time,
      date:util.myformatTime(new Date()),
      renyishiduan:this.data.renyishiduan,
      zaochenxiguan:this.data.zaochenxiguan,
      zhongwuxiguan:this.data.zhongwuxiguan,
      xiawuxiguan:this.data.xiawuxiguan,
      timeLineData:this.data.timeLineData,
      demo5_days_style:this.data.demo5_days_style,
      totalAbsorbedTime:this.data.totalAbsorbedTime,
      zhuanzhucishu:this.data.zhuanzhucishu
    },
    success:res=>{
      console.log("数据更新成功"+res)
    },
    fail:err=>{
      wx.showToast({
        title: '数据异常',
        icon:'none'
      })
    }
  })

  this.dakatianshu()
  app.globalData.globalrenyi=this.data.renyishiduan
  app.globalData.globalzaochen=this.data.zaochenxiguan
  app.globalData.globalzhongwu=this.data.zhongwuxiguan
  app.globalData.globalxiawu=this.data.xiawuxiguan

},

//全力爆发期
quanlibaofa(){
  this.setData({
    dangqianjieduan:{       
    imgUrl:"https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/yuanqi.jpg?sign=1fffd635197765e69bf2f4d3935a0ec8&t=1587989609",
    name:"全力爆发期",
    description:"培养阶段性习惯的第一个阶段，通常指前三天，这个时期主要是我们运用意志力的时候，此时我们的潜意识有巨大的能量，你必须要专注于某件事，其次要避免用力过度，要阶段性增加难度，才能事半功倍"}
  })
},
//意志疲软期
yizhipiruan(){
  this.setData({
    dangqianjieduan:{       
      imgUrl:"https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/pibei.jpeg?sign=c273964d46697d4225de385a18105f06&t=1588432215",
      name:"意志疲软期",
      description:"通常指第四天到第七天，我们常会在意志疲软期遭遇失败，主要三个原因，一、用力过度，在全力爆发期的时候用力过度；二、不够聚焦；三、没有成就感，没有这种奖励的感觉。只要避免这三种情况，通常就能够顺利的度过意志疲软期"}
  })
},
//懈怠麻木期
xiedaimamu(){
  this.setData({
    dangqianjieduan:{       
      imgUrl:"https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/mamu.jpeg?sign=23d6f289646598fc0dff62a7ba99735d&t=1589543195",
      name:"懈怠麻木期",
      description:"常指8到21天，此时期你的践行不太需要你的意志力，并且习惯已经有了雏形，有点惯性的感觉，你会很容易放松，也很容易麻木。过渡期我们需要一直提醒自己警惕，继续这种非常用心的一种践行，尽量不要让自己麻木，就可以避免这个危险期"}
  })
},
//平稳期
pingwen(){
  this.setData({
    dangqianjieduan:{       
      imgUrl:"https://7465-test-e5nc7-1300496285.tcb.qcloud.la/upload/richang/pingwenqi.jpg?sign=0fdc04c839f2737b60ce02a8a07adb07&t=1589543705",
      name:"平稳期",
      description:"恭喜你已经进入最后一个阶段——平稳期，这个时期实际上是很舒服的，但是不可排除也有失败的可能，这个就是将我们过渡到未来的固化期的一个桥梁，一定要根据理念能够爱上这个践行，这才可能越来越平稳。"}
  })
},
xiguanweikong(){
  if(!this.data.renyishiduan[0]&&!this.data.zaochenxiguan[0]&&!this.data.zhongwuxiguan[0]&&!this.data.xiawuxiguan[0]){
    this.setData({
      demo5_days_style:[],
      totalAbsorbedTime:0
    })
  }
},
//长按删除
DeleteRen(e){
    wx.showModal({
    cancelColor: 'cancelColor',
    title:'提示',
    content:'是否删除该习惯',
    success:res=>{
      if(res.confirm){
      console.log("用户删除")
      let renyishiduan=this.data.renyishiduan
      renyishiduan.splice(e.target.dataset.renindex,1)
      this.setData({
        renyishiduan
      })
      db.collection('demo').doc(this.data._id).update({
        data:{
          renyishiduan
        },
        success:res=>{
          console.log(res)
        }
      })
    
    }else if(res.cancel){
       console.log("未删除")
    }
  }
  })
  this.xiguanweikong()
},
DeleteZao(e){
   wx.showModal({
    cancelColor: 'cancelColor',
    title:'提示',
    content:'是否删除该习惯',
    success:res=>{
      if(res.confirm){
      console.log("用户删除")
      let zaochenxiguan=this.data.zaochenxiguan
      zaochenxiguan.splice(e.target.dataset.zaoindex,1)
      this.setData({
        zaochenxiguan
      })
      db.collection('demo').doc(this.data._id).update({
        data:{
          zaochenxiguan
        },
        success:res=>{
          console.log(res)
        }
      })
    
    }else if(res.cancel){
       console.log("未删除")
    }
  }
  })
  this.xiguanweikong()

},
DeleteZhong(e){
  wx.showModal({
   cancelColor: 'cancelColor',
   title:'提示',
   content:'是否删除该习惯',
   success:res=>{
     if(res.confirm){
     console.log("用户删除")
     let zhongwuxiguan=this.data.zhongwuxiguan
     zhongwuxiguan.splice(e.target.dataset.zhongindex,1)
     this.setData({
      zhongwuxiguan
     })
     db.collection('demo').doc(this.data._id).update({
       data:{
        zhongwuxiguan
       },
       success:res=>{
         console.log(res)
       }
     })
   
   }else if(res.cancel){
      console.log("未删除")
   }
 }
 })
 this.xiguanweikong()

},
DeleteXia(e){
  wx.showModal({
   cancelColor: 'cancelColor',
   title:'提示',
   content:'是否删除该习惯',
   success:res=>{
     if(res.confirm){
     console.log("用户删除")
     let xiawuxiguan=this.data.xiawuxiguan
     xiawuxiguan.splice(e.target.dataset.xiaindex,1)
     this.setData({
      xiawuxiguan
     })
     db.collection('demo').doc(this.data._id).update({
       data:{
        xiawuxiguan
       },
       success:res=>{
         console.log(res)
       }
     })
   
   }else if(res.cancel){
      console.log("未删除")
   }
 }
 })
 this.xiguanweikong()

},

  fanqie(){
    console.log(this.data.habbitname)
   var habbitname=this.data.habbitname
   wx.navigateTo({
     url: '../fanqie/fanqie?habbitname='+habbitname,
   })
  },
  savaSuccessShow(){
    this.setData({
      savaSuccess:false
    })
  },

})