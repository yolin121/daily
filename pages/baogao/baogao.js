// pages/baogao/baogao.js
Page({

  data: {
    isShow:true,
    val:"",
    taskid:1,
    
    todolist_data:[
      {
      id:1,
      icon:"",
      title:"学英语",
      tags:"20分钟"
    }
  ]
  },
  onLoad: function (options) {

  },
  //判断有无内容
  addtask:function(){
    //有内容
    if(this.data.val){
    var todolist_data=this.data.todolist_data
    var list={
      id:2,
      icon:"",
      title:this.data.val,
      tags:this.data.tags,
      animate1:"slideInLeft 1.5s",
      isModalHidden:true
    }
    todolist_data.push(list)
    var that=this
    that.setData({
      todolist_data:todolist_data,
      isShow:false,
      // 
      val:""
    })
    wx.showToast({
      title: '添加成功',
      icon:'success'
    })
    }
    else{
      console.log("未填写内容")
    }
    this.setData({
      isShow:true,
    })
  },

  //点击新建内容
  create:function(){
     this.setData({
      isShow:false
    })
  },

  //将组件传过来的值赋给val
  addval:function(e){
    // console.log(e.detail.isval)
    // console.log(typeof(e.detail.isval))
    this.setData({
      val:e.detail.isval
    })
  },
  //关闭
  addclose:function(){
    this.setData({
      isShow:true
    })
  },
  //时间
  picker_time:function(e){
    console.log(e.detail)
    if(e.detail==0){
      this.setData({
        tags:"15分钟"
      })
    }else if(e.detail==1){
      this.setData({
        tags:"30分钟"
      })
    }else if(e.detail==2){
      this.setData({
        tags:"45分钟"
      })
    }else if(e.detail==3){
      this.setData({
        tags:"60分钟"
      })
    }else if(e.detail==4){
      this.setData({
        tags:"90分钟"
      })
  }
  },
  delTodo:function(){
    wx.showToast({
      title: '你要干撒？？？',
      icon:'loading'
    })
  }
})