var util=require('../../utils/util.js')
const db=wx.cloud.database();
var app=getApp();
Page({


  data: {
    textare:false,
    min: 0,
    max: 150,
    time:'',
    // cont:'',
    content:'',
    img:'',
    suiji:'',
    journalitem:[{time:'2020/06/01 12:00',detail:'记录生活，记录成长',img:'cloud://test-e5nc7.7465-test-e5nc7-1300496285/richang/first.jpg'},]
    
  },

  inputs: function (e) {
    console.log(e.detail.value)
    var value = e.detail.value;
    var len = parseInt(value.length);
    if (len > this.data.max) return;

    this.setData({
      currentWordNumber: len,
      content:e.detail.value
    });
    if(this.data.currentWordNumber == 150){
      wx.showModal({
        title: '提示',
        content: '您输入的次数已达上限',
      })
    }
  
  },

  //获取当前时间
  getTime(){
    // 获取util时间
    var TIME=util.journal(new Date());
    console.log(TIME)
    this.setData({
      time:TIME
    }) 

 },
  onLoad: function (options) {
    wx.showToast({
      title: '正在刷新数据',
      icon:'loading'
    })
    db.collection('demo').where({
      _openid:app.globalData.userOpenid                                                                                                         
    }).get().then(res=>{
      console.log(res)
      this.setData({
        _id:res.data[0]._id
      })
      if(!res.data[0].journalitem){  //没有journal
          console.log("数据库无journalitem")
          this.update()
      }else{
      this.setData({
        journalitem:res.data[0].journalitem
      })
    }
    })
    
  },

  update(){
    db.collection('demo').doc(this.data._id).update({
      data:{
        journalitem:this.data.journalitem
      },success:res=>{
        console.log("更新数据成功"+res)
        wx.showToast({
          title: '更新数成功',
        })
      },fail:err=>{
        wx.showToast({
          title: '数据异常',
          icon:'none'
        })
      }
    })
  }
  ,
  baocun(e){  
    
      if(this.data.content==''){
        wx.showToast({
          title: '内容为空',
          icon:'none'
        })
        return;
      }
      this.getTime()
      var  journalitem=this.data.journalitem
      journalitem.unshift({
        time:this.data.time,
        detail:this.data.content,
        img:this.data.img
      })
      
      this.setData({
        textare:false,
        journalitem,
        content:''
      })
      setTimeout(res=>{this.update()},500)
     
   
  },

  quxiao(){
    this.setData({
      textare:false,
    })
  },
  back(){
    wx.navigateBack({
      complete: (res) => {},
    })
  },
  write(){
    this.setData({
      textare:true,
      img:''
    })
  },
  //失去焦点
//   bindTextAreaBlur(e){
//     console.log(e.detail.value)
//     this.setData({
//       cont: e.detail.value,
//       content:''
     
//  })

//   },
suijishu(){
  var suiji=Math.floor(Math.random()*100000)
  console.log(suiji)
  this.setData({
    suiji
  })
},
upimg(){
  this.suijishu()
  //弹出选择图片对话框
  wx.chooseImage({
    complete: (res) => {
      console.log(res)
      let path=res.tempFilePaths[0]
     wx.showToast({
       title: '图片上传中',
       icon:'loading',
       duration:4000,
       mask:true
     })
      //云存储调用-上传文件方法
      wx.cloud.uploadFile({
        cloudPath:"richang/"+this.data.suiji+".png",
        filePath:path,
        success:(res)=>{
          console.log("图片上传成功",res)
          wx.showToast({
            title: '图片上传成功',
          })
          this.setData({
            img:res.fileID
          })
        },
        fail:(res)=>{
          console.log("上传失败",res)

        }
      })

    },  
  })
},
  //删除
  delete(e){
    console.log(e.target.dataset.index)
    wx.showModal({
      title: '提示',
      content: '是否删除该内容？',
      success :res=> {
      if (res.confirm) {
      console.log('用户点击确定')
       var journalitem=this.data.journalitem
       journalitem.splice(e.target.dataset.index,1)
       this.setData({
         journalitem
       })
       this.update()
      } else if (res.cancel) {
      console.log('用户点击取消')
      }
      }
      })
  },

  onReady: function () {
    
  },


  onShow: function () {
    
  },


  onHide: function () {
    
  },


  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})