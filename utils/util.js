const formatTime = date => {
 //const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  //const second = date.getSeconds()

  return [ month, day].map(formatNumber).join('/') + ' ' + [hour, minute].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}


const myformatTime = date => {
  //const year = date.getFullYear()
   const month = date.getMonth() + 1
   const day = date.getDate()
   //const hour = date.getHours()
   //const minute = date.getMinutes()
   //const second = date.getSeconds()
 
   return [ month, day].map(myformatNumber).join('/')
 }
 
 const myformatNumber = n => {
   n = n.toString()
   return n[1] ? n : '0' + n
 }
 //番茄
const formatTime2 = date => {
  const year = date.getFullYear()
   const month = date.getMonth() + 1
   const day = date.getDate()
   const hour = date.getHours()
   const minute = date.getMinutes()
   const second = date.getSeconds()
   return [ year,month, day].map(formatNumber1).join('/') + ' ' + [hour, minute,second].map(formatNumber1).join(':')
 }
 
 const formatNumber1 = n => {
   n = n.toString()
   return n[1] ? n : '0' + n
 }

 const journal = date => {
  const year = date.getFullYear()
   const month = date.getMonth() + 1
   const day = date.getDate()
   const hour = date.getHours()
   const minute = date.getMinutes()
   return [ year,month, day].map(formatNumber1).join('/') + ' ' + [hour, minute].map(formatNumber1).join(':')
 }
 
//  const formatNumber1 = n => {
//    n = n.toString()
//    return n[1] ? n : '0' + n
//  }
const myhour = date => {
   const hour = date.getHours()
   return [hour]
 }
module.exports = {
  formatTime: formatTime,
  myformatTime:myformatTime,
  formatTime2:formatTime2,
  journal:journal,
  myhour
}

